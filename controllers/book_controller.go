package controllers

import (
	"context"
	"net/http"
	"time"

	"github.com/gautamprikshit1/library-manager-backend/configs"
	"github.com/gautamprikshit1/library-manager-backend/models"
	"github.com/gautamprikshit1/library-manager-backend/responses"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

var (
	booksCollection *mongo.Collection = configs.GetCollection(configs.DB, "Books")
	validate                          = validator.New()
)

func CreateBook() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		var book models.Book
		defer cancel()

		if err := c.BindJSON(&book); err != nil {
			c.JSON(http.StatusBadRequest, responses.Response{
				Status:  http.StatusBadRequest,
				Message: "Error: Bad Request",
				Data:    map[string]interface{}{"data": err.Error()},
			})
			return
		}

		if validationErr := validate.Struct(&book); validationErr != nil {
			c.JSON(http.StatusBadRequest, responses.Response{
				Status:  http.StatusBadRequest,
				Message: "Error: Bad Request",
				Data:    map[string]interface{}{"data": validationErr.Error()},
			})
			return
		}

		newBook := models.Book{
			ID:       primitive.NewObjectID(),
			Title:    book.Title,
			IsIssued: book.IsIssued,
			Author:   book.Author,
		}

		result, err := booksCollection.InsertOne(ctx, newBook)

		if err != nil {
			c.JSON(http.StatusInternalServerError, responses.Response{
				Status:  http.StatusInternalServerError,
				Message: "Internal Server Error",
				Data:    map[string]interface{}{"data": err.Error()},
			})
		}
		c.JSON(http.StatusOK, responses.Response{
			Status:  http.StatusOK,
			Message: "success",
			Data:    map[string]interface{}{"data": result},
		})
	}
}

func GetBook() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		bookId := c.Param("bookId")
		var book models.Book
		defer cancel()

		objId, _ := primitive.ObjectIDFromHex(bookId)

		err := booksCollection.FindOne(ctx, bson.M{"id": objId}).Decode(&book)
		if err != nil {
			c.JSON(http.StatusInternalServerError, responses.Response{
				Status:  http.StatusInternalServerError,
				Message: "Internal Server Error",
				Data:    map[string]interface{}{"data": err.Error()},
			})
			return
		}
		c.JSON(http.StatusOK, responses.Response{
			Status:  http.StatusOK,
			Message: "success",
			Data:    map[string]interface{}{"data": book},
		})
	}
}

func EditBook() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		bookId := c.Param("bookId")
		var book models.Book
		defer cancel()
		objId, _ := primitive.ObjectIDFromHex(bookId)

		if err := c.BindJSON(&book); err != nil {
			c.JSON(http.StatusBadRequest, responses.Response{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Data:    map[string]interface{}{"data": err.Error()},
			})
			return
		}

		if validationErr := validate.Struct(&book); validationErr != nil {
			c.JSON(http.StatusBadRequest, responses.Response{
				Status:  http.StatusBadRequest,
				Message: "Bad request",
				Data:    map[string]interface{}{"data": validationErr.Error()},
			})
			return
		}
		update := bson.M{"title": book.Title, "is_issued": book.IsIssued, "author": book.Author}
		result, err := booksCollection.UpdateOne(ctx, bson.M{"id": objId}, bson.M{"$set": update})
		if err != nil {
			c.JSON(http.StatusInternalServerError, responses.Response{
				Status:  http.StatusInternalServerError,
				Message: "Internal server error",
				Data:    map[string]interface{}{"data": err.Error()},
			})
			return
		}

		var updatedBook models.Book
		if result.MatchedCount == 1 {
			if err := booksCollection.FindOne(ctx, bson.M{"id": objId}).Decode(updatedBook); err != nil {
				c.JSON(http.StatusInternalServerError, responses.Response{
					Status:  http.StatusInternalServerError,
					Message: "Internal server error",
					Data:    map[string]interface{}{"data": err.Error()},
				})
				return
			}
		}
		c.JSON(http.StatusOK, responses.Response{
			Status:  http.StatusOK,
			Message: "success",
			Data:    map[string]interface{}{"data": updatedBook},
		})
	}
}

func GetAllBooks() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		var books []models.Book
		defer cancel()

		results, err := booksCollection.Find(ctx, bson.M{})
		if err != nil {
			c.JSON(http.StatusInternalServerError, responses.Response{
				Status:  http.StatusInternalServerError,
				Message: "Internal Server Error",
				Data:    map[string]interface{}{"data": err.Error()},
			})
			return
		}

		defer results.Close(ctx)
		for results.Next(ctx) {
			var singleBook models.Book
			if err = results.Decode(&singleBook); err != nil {
				c.JSON(http.StatusInternalServerError, responses.Response{
					Status:  http.StatusInternalServerError,
					Message: "Internal Server Error",
					Data:    map[string]interface{}{"data": err.Error()},
				})
			}

			books = append(books, singleBook)
		}
		c.JSON(http.StatusOK, responses.Response{
			Status:  http.StatusOK,
			Message: "success",
			Data:    map[string]interface{}{"data": books},
		})
	}
}

func DeleteBook() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		bookId := c.Param("bookId")
		defer cancel()

		objId, _ := primitive.ObjectIDFromHex(bookId)

		result, err := booksCollection.DeleteOne(ctx, bson.M{"id": objId})
		if err != nil {
			c.JSON(http.StatusInternalServerError, responses.Response{
				Status:  http.StatusInternalServerError,
				Message: "Internal Server Error",
				Data:    map[string]interface{}{"data": err.Error()},
			})
			return
		}
		if result.DeletedCount < 1 {
			c.JSON(http.StatusNotFound, responses.Response{
				Status:  http.StatusNotFound,
				Message: "Not found",
				Data:    map[string]interface{}{"data": "Book with specified Id not found"},
			})
			return
		}

		c.JSON(http.StatusOK, responses.Response{
			Status:  http.StatusOK,
			Message: "success",
			Data:    map[string]interface{}{"data": "Book deleted successful"},
		})
	}
}
