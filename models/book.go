package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Book struct {
	ID       primitive.ObjectID `json:"id,omitempty"`
	Title    string             `json:"title, omitempty" validate:"required"`
	IsIssued bool               `json:"is_issued, omitempty" validate:"required"`
	Author   Person             `json:"author, omitempty" validate:"required"`
}

type Person struct {
	FirstName string
	LastName  string
}
