package main

import (
	"net/http"

	"github.com/gautamprikshit1/library-manager-backend/configs"
	"github.com/gautamprikshit1/library-manager-backend/models"
	"github.com/gautamprikshit1/library-manager-backend/routes"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

var books []models.Book

func getBooks(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, books)
}

func addBook(ctx *gin.Context) {
	var book models.Book
	if err := ctx.BindJSON(&book); err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "error reading json.",
		})
	}
	books = append(books, book)
}

func main() {
	router := gin.Default()
	router.Use(cors.Default())

	configs.ConnectDB()
	routes.BookRoute(router)
	router.Run(":5000")
}
