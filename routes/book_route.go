package routes

import (
	"github.com/gautamprikshit1/library-manager-backend/controllers"
	"github.com/gin-gonic/gin"
)

func BookRoute(router *gin.Engine) {
	router.POST("/book", controllers.CreateBook())
	router.GET("/book/:id", controllers.GetBook())
	router.PUT("/book/:id", controllers.EditBook())
	router.DELETE("/book", controllers.DeleteBook())
	router.GET("/books", controllers.GetAllBooks())
}
